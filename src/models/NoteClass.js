'use strict';

const  uuidv1 = require('uuid/v1');

class NodeClass {
    constructor() {
        this.name = '';
        this.text = '';
        this.uuid = uuidv1();
        this.status = 'new';
        this.order = '';
        this.color = 'white';
        this.list = false;
        this.itemList = '';
    }
}

export default NodeClass

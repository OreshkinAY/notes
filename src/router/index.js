import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Note from '@/components/Note'
import Archive from '@/components/Archive'
//import VueDragDrop from 'vue-drag-drop';


Vue.use(Router)
//Vue.use(VueDragDrop);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/note/:id',
      name: 'Note',
      component: Note
    },
    {
      path: '/archive',
      name: 'Archive',
      component: Archive
    }
  ]
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import lodash from 'lodash' // чтобы млжно было пользоваться return _.orderBy
import VueDragDrop from 'vue-drag-drop';

Vue.config.productionTip = false

import DragControlBlock from '@/components/drag-control-block.vue'
import DragBlock from '@/components/drag-block.vue'
import Search from '@/components/Search.vue'
import Notes from '@/components/notes.vue'



import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";

Vue.use(VueMaterial);
Vue.use(VueDragDrop);

Vue.component('drag-control-block', DragControlBlock)
Vue.component('drag-block', DragBlock)
Vue.component('search', Search)
Vue.component('notes', Notes)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store : store
})

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
import NodeClass from '../models/NoteClass'

const loadState = (key) => {
    try {
        const serializedState = localStorage.getItem(key);
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

const saveState = (state, key) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem(key, serializedState);
    } catch (err) {
        console.error(`Something went wrong: ${err}`);
    }
}


const store = new Vuex.Store({
        state: {
            //notes: [],
            notes: loadState('notes') || [],
            colors: [
                {name: 'White'},
                {name: 'Lightgreen'},
                {name: 'Skyblue'},
            ],
        },
        actions: {
            addNote({commit}, note) {
                commit('ADD_NOTE', note)
            },
            noteEdit({commit}, note) {
                commit('EDIT_NOTE', note)
            },

            formatList({commit}, note) {
                commit('FORMAT_LIST', note)
            },

            delNote({commit}, note) {
                commit('DEL_NOTE', note)
            },
            reorder({commit, state}, nodes) {
                commit('REORDER_NOTE', nodes)
                saveState(state.notes, 'notes');
            }
        },
        mutations: {
            ADD_NOTE(state, note) {
                note.order = state.notes.length + 1;
                state.notes.push(note)
                saveState(state.notes, 'notes');
            },

            EDIT_NOTE(state, note) {

                function isPrime(element, index) {
                    if (element.uuid === note.uuid) {
                        state.notes[index].name = note.name.trim();
                        state.notes[index].text = note.text.trim();
                    }
                }

                const result = state.notes.find(isPrime);
                saveState(state.notes, 'notes');
            },

            FORMAT_LIST(state, note) {
                saveState(state.notes, 'notes');
            },

            DEL_NOTE(state, note) {
                function isPrime(element, index) {
                    // после удаления find пробегается по массиву и не находит первый элемент = undefined
                    if (typeof element !== "undefined") {
                        if (element.uuid === note) {
                            Vue.delete(state.notes, index)
                        }
                    }
                }

                const result = state.notes.find(isPrime);
                saveState(state.notes, 'notes');
            }
            ,
            REORDER_NOTE(state, nodes) {
                state.notes = nodes;
            }
            ,
        }
        ,
        getters: {
            getNotes(state) {
                function isNew(value) {
                    return value.status === 'new';
                }

                const result = state.notes.filter(isNew);

                return result;
            },
            getNote: state => url => {
                const result = state.notes.find(item => item.uuid === url);

                return result
            },
            getNoteSort:
                state => sort => {
                    const result = state.notes.find(item => item.order == sort);

                    return result
                },
            getNotesArchived(state) {
                function isArchived(value) {
                    return value.status === 'archived';
                }

                const result = state.notes.filter(isArchived);

                return result;
            },
            getNotesAll(state) {
                return state.notes;
            },

        }
    })
;

export default store
